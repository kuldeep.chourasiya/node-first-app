const mysql = require('mysql');
const express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');
const db = require('./db/mysqlConnection');
const routes = require('./route/routesMysql');
const app = express();
app.use(bodyParser.json());

// Set up Multer for handling file uploads
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/');
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + '-' + file.originalname);
  },
});

const upload = multer({ storage: storage });

app.use(express.static('public'));
 
app.use('/', routes(upload,db));

app.listen(3000, () => {
  console.log('Server started on port 3000');
});