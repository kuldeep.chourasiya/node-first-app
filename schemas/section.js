const mongoose = require('mongoose');

const sectionSchema = new mongoose.Schema({
    name: String
});

const Sections = mongoose.model('Section', sectionSchema);

module.exports = Sections;