const express = require('express');
const router = express.Router();
const db = require('../db/cloudMongodb'); 
router.post('/users', async (req, res) => {
  const { name, email, age } = req.body;

  try {
    const database = await db.connectToDatabase(); // Import your db.js file
    const collection = database.collection('user');
    const user = { name, email, age };
    await collection.insertOne(user);
    res.send(user);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
});

module.exports = router;