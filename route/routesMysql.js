const express = require('express');
const router = express.Router();

module.exports = (upload,db) => {
    router.post('/upload', upload.array('files', 5), (req, res) => {
        console.log(req.files);
        res.send('Files uploaded successfully.');
    });
    
    router.get('/fileupload', (req, res) => {
        res.sendFile(__dirname + '/index.html');
    });
    
    
    router.get('/fetch-data', (req, res) => {
        // Perform a MySQL query to retrieve data
        db.query('SELECT * FROM customers', (error, results) => {
            if (error) {
            console.error('Error retrieving data:', error);
            res.status(500).json({ error: 'Database error' });
            } else {
            res.json(results);
            }
        });
    });
    
    router.post('/addData', upload.single('file'), (req, res) => {
        const data = req.body;
        const file = req.file; 

        const query = 'INSERT INTO customers (name, address,file) VALUES (?, ?, ?)';
        const values = [data.name, data.address, file.filename]; 

        db.query(query, values, (error, results) => {
            if (error) {
            console.error('Error inserting data:', error);
            res.status(500).json({ error: 'Database error' });
            } else {
            res.json({ message: 'Data inserted successfully' });
            }

        });
    });

    return router;
}
