const express = require('express');
const User = require('../schemas/user');
const Product = require('../schemas/product');
const SectionsSelect = require('../schemas/section');
const router = express.Router();

// Create a new user
router.post('/users', async (req, res) => {
  const { name, email, age } = req.body;

  try {
    const user = new User({ name, email, age });
    await user.save();
    res.send(user);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
});

// Get all users
router.get('/users', async (req, res) => {
  try {
    const users = await User.find({});
    res.send(users);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
});

// Update a user
router.put('/users/:id', async (req, res) => {
  const { id } = req.params;
  const { name, email, age } = req.body;

  try {
    const user = await User.findByIdAndUpdate(id, { name, email, age }, { new: true });
    res.send(user);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
});

// Delete a user
router.delete('/users/:id', async (req, res) => {
  const { id } = req.params;

  try {
    const user = await User.findByIdAndDelete(id);
    res.send(user);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
});


//Create a new product
router.post('/products', async (req, res) => {
  const { name, price, unit } = req.body;

  try {
    const product = new Product({ name, price, unit });
    await product.save();
    res.send(product);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
});

router.get('/products', async (req, res) => {
    try {
      const products = await Product.find({});
      res.send(products);
    } catch (error) {
      console.error(error);
      res.status(500).send(error);
    }
});

router.post('/section', async (req, res) =>{
  const { name } = req.body;

  try{
      const section = new SectionsSelect({ name });
      await section.save();
      res.send(name);
  } catch{
      console.error(error);
      res.status(500).send(error);
  }
});

router.get('/section', async (req, res) => {
  try {
    const section = await SectionsSelect.find({});
    res.send(section);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
});
module.exports = router;