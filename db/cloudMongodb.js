const { MongoClient, ServerApiVersion } = require('mongodb');
const uri = "mongodb+srv://kuldeep00751:83Qr0C5SUx1PcAKp@cluster1.vfdgc2d.mongodb.net/?retryWrites=true&w=majority";

const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

async function connectToDatabase() {
  try {
    await client.connect();
    console.log('Connected to MongoDB');
    const database = client.db('nodeApp');
    return database;
  } catch (error) {
    console.error('Error connecting to MongoDB:', error);
    throw error;
  }
}

module.exports = {
  connectToDatabase
};