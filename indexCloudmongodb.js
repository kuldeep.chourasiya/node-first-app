const express = require('express');
const db = require('./db/cloudMongodb'); 
const app = express();
app.use(express.json());
app.listen(3000, async () => {
  try {
    const database = await db.connectToDatabase();
    console.log('Server is running on port 3000');
    // Start using the 'database' object in your routes
  } catch (error) {
    console.error('Failed to connect to the database:', error);
  }
});

// Define your routes
app.use('/', require('./route/routesCloudmongodb'));